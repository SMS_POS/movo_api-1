require 'sinatra'
require 'nori'
#require "webrick/https"
#require "webrick/ssl"
#require "openssl"

module Api

  class AndroidApi < Sinatra::Base
    use Rack::CommonLogger
    #register Sinatra::HTTPAuth
    #http_basic_authenticate_with :name => "admin", :password => "secret"

    use Rack::Auth::Basic, "Protected Area" do |username, password|
      @username = username
      $log.info password
      $log.info Digest::MD5.hexdigest("admin")
      username == 'Admin' && password == Digest::MD5.hexdigest("admin")
    end


    configure do
       set(:server, 'thin')
       set(:port,6081)
       set(:bind,'0.0.0.0')
       set(:logging,true)
    end

    before do
      Api::Connection.connect
    end

    after do
      Api::Connection.disconnect
    end


    ##post '/v1/login' do
    #  raw = request.env["rack.input"].read
    #  #raw = raw.to_s.gsub!(/\n/, "").gsub!(/>\s*</, "><")
    #  pp raw
    #  parser = Nori.new
    #  response_hash = parser.parse(raw)
    #
    #  username = response_hash["Request"]["LoginPost"]["Username"]
    #  password = response_hash["Request"]["LoginPost"]["Password"]
    #
    #  if (username == "adrian.toman" && password == "kokos1" )
    #    builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
    #      xml.Response do
    #        xml.SessionId "adsa6d4a6sd46as4d6ad6as4d"
    #        xml.Message "Login was succesfull"
    #      end
    #    end
    #    builder.to_xml
    #  else
    #    builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
    #      xml.Response do
    #        xml.Error "Login failed"
    #        xml.Message "You have provided wrong username or password"
    #      end
    #    end
    #    builder.to_xml
    #
    #  end
    #  builder.to_xml
    #end

    get '/v1/login' do
      builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
        xml.Response do
          xml.SessionId "adsa6d4a6sd46as4d6ad6as4d"
          xml.Message "Login was succesfull"
        end
      end
      builder.to_xml
    end

    get '/v1/viewers' do
      serial_number   = params[:serial_number]
      viewers = Viewers.where("viewers_smartcard_id LIKE ?","%#{serial_number}%")
      viewers.to_json
    end


    get '/v1/products' do
      products = Products.find_active
      products.to_json
    end

    post '/v1/invoices' do
      raw = request.env["rack.input"].read
      json = JSON.parse(raw)
      begin
        Viewers_invoices.create({"viewers_id" => json["viewer_id"],"product_id" => json["product_id"],"text" => json["text"],"user_id" => json["user_id"]})
        $log.info "Invoice save successfull"
        {"response" => "SUCCESS"}
      rescue => e
        $log.error "Invoice save failed #{json.to_s} \n #{e.message}"
        {"response" => "ERROR", "message" => e.message}
      end
    end


    get '/v1/test' do
      builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
        xml.Response do
          xml.SessionId "adsa6d4a6sd46as4d6ad6as4d"
          xml.Message "Login was succesfull"
        end
      end
      builder.to_xml
    end

    get '/v1/test2' do
      "test"
    end





  end

end
